(ns pubparse.core)

(def sample-key "AAAAB3NzaC1yc2EAAAADAQABAAABAQDM8C7TJGPWKEC7X1SLx2YmwOYOYj0o5+s3xutz3vlEDjXHDylixSBTzSJqHnsCKvHT1QLpyTXXNBfORRhDHrEeU5I+3HKy1hcaAnZ8Om6Pi8TdpdOvZqL0xG+05Drt8P3WMb0Aw4B6CWV3BgMcbbYOnXvAKu/7wB3yDQ6yocGD5iWXR3r+2fuurR8XJ5QpD+XSmqmE4MzflWDNOdJmXcJGF4XHTx5iLCWeJYqruCDciyCzsTiTZqNtHaZlJsNdCKlKrhy2Gdlqo7BKHaRQmRDgilIDdvr4LsR3gOgUAEoSwKlES1xM6d5uCDTgTOcE3Z9S3HQ8rF89dxHZ6CmoTIpF")

(defn uint [b]
  (Byte/toUnsignedInt b))

(defn base64-decode
  "decode a base64 string into a seq of ints"
  [b64-text]
  (->> b64-text
       (.decode (java.util.Base64/getDecoder))
       (map uint)))

(defn bytes->bignum
  "convert a seq of ints into a big number (network order)"
  [bs]
  (reduce (fn [acc n] (+' (*' acc 256) n))
          0
          bs))

(defn bytes->str
  "convert a seq of ints into a string"
  [bs]
  (String. (byte-array bs)))

(defn split-parts
  "break up a seq of ints into packets, assuming each packet is encoded
  as a 4-byte length field (encoding a number N) followed by N bytes"
  [bs]
  (when (seq bs)
    (let [n (bytes->bignum (take 4 bs))
          [part more] (split-at n (drop 4 bs))]
      (cons part (split-parts more)))))

(defn parse-ssh-pubkey
  "parse an ssh pubkey in base64, assuming type is ssh-rsa"
  [pubkey]
  (let [[ktype e n] (split-parts (base64-decode pubkey))]
    {:type (bytes->str ktype)
     :e (bytes->bignum e)
     :n (bytes->bignum n)}))
